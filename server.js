'use strict';

const express = require('express');

const PORT = process.env.PORT || 3000;

const app = express();

const expressWs = require('express-ws')(app);



function makeMessage(action, data) {
    var resp = {
        action: action,
        data: data
    };

    return JSON.stringify(resp);
}


let nextID = 1;
let rooms = {};

console.log('almost');
app.ws('/:roomID', (ws, req) => {

	let roomID = req.params.roomID;
	let userID = nextID;

	nextID++;

	if(rooms.hasOwnProperty(roomID))
	{
		console.log('room exists');
		if(rooms[roomID].players.length < 2)
		{
			rooms[roomID].players.push({userID: userID, ws: ws, p: 2, ready: false});
			rooms[roomID].players.forEach((player) => {
				player.ws.send(makeMessage('gameStart', ((player.p == 1) ? 'chooseWord' : 'waitForWord')));
			});
		}
		else
		{
			ws.send(makeMessage('roomFull', roomID));
		}
	}
	else
	{
		console.log('creating room');
		rooms[roomID] = {players: [{userID: userID, ws: ws, p: 1, ready: false}], word: [], currentWord: [], wrongGuesses: 0};
		ws.send(makeMessage('waiting', 'data'));
	}
	
	const room = rooms[roomID];

	ws.on('message', function(msg) {

		console.log('received from ' + userID + ': ' + msg) 
		//userID and roomID are stored somehow
	    try
	    {
	        var msg = JSON.parse(msg);
	    }
	    catch (error) 
	    {
	        ws.send(makeMessage('error', 'Invalid action'));
	        return;
	    }

		function sendToRoom(action, data) 
		{
			room.players.forEach((player) => {
				player.ws.send(makeMessage(action, data));
			});	
		}

	    try 
	    {
	        switch (msg.action)
	        {
	            case 'wordSelect':
	            	room.word = msg.data.toUpperCase().split('');
	            	room.currentWord = room.word.map((letter) => {
	            		return ' ';
	            	});
	            	room.players.forEach((player) => {
						player.ws.send(makeMessage((player.p == 1) ? 'watching' : 'playing', room.currentWord));
					});	
		            break;

		        case 'sendLetter':   //not the best name for this, as you are receiving
		
		        	if(room.word.indexOf(msg.data) != -1)  //letter is in the word
		        	{
		        		room.currentWord = room.currentWord.map((letter, i) => 
		        		{
		        			if(letter != ' '){ return letter; }          //add guessed letter to word if its in the word
		        			else if(msg.data == room.word[i]) { return room.word[i]; }   
		        			else { return ' '; }
		        		});

		        		if(room.currentWord.indexOf(' ') == -1)  //if word is complete now
		        		{
		 					//add point calculation here, based on time maybe? 
		        			sendToRoom('gameOver', {winner: true, word: room.currentWord, letter: msg.data})
		        		}
		        		else
		        		{
			        		sendToRoom('true', {word: room.currentWord, letter: msg.data})
						}
		        	}
		        	else   //currentWord stays the same
		        	{
		        		room.wrongGuesses += 1;
		        		if(room.wrongGuesses === 6)
		        		{
		        			sendToRoom('gameOver', {winner: false, word: room.word, letter: msg.data}); //0 points for player
		        		}
		        		else { sendToRoom('false', {letter: msg.data, wrongGuesses: room.wrongGuesses}); }
		        	}
		        	break;

			    case 'switchSides':

		        	if(room.players[0].p == msg.data)
		        	{
		        		room.players[0].ready = true;
		        	}
		        	else
		        	{
		        		room.players[1].ready = true;
		        	}
		        	if(room.players[0].ready === true && room.players[1].ready === true)
		        	{
	        			room.word = [];
	        			room.currentWord = [];
	        			room.wrongGuesses = 0;
	        			room.players.forEach((player) => {
	        				player.p = (player.p) == 2 ? 1 : 2;
	        				player.ready = false;
							player.ws.send(makeMessage('gameStart', ((player.p == 1) ? 'chooseWord' : 'waitForWord')));
						});		        		
		        	}
		        	break;
		        default:
		        	console.log('hitting default, action not found');
		        	break;
	        }
	    } 
	    catch (error)
	    {
	        ws.send(makeMessage('error', error.message));
	    }
	});

 	ws.on('close', () => {

	  	console.log(userID + ' disconnected');
	  	
	  	room.wrongGuesses = 0;
	  	room.currentWord = [];        //reset room
	  	room.word = [];

	  	if(room.players.length < 2)
	  	{
	  		delete rooms[roomID];
	  	}
	  	else
	  	{
		  	if(room.players[0].userID == userID)
		  	{
		  		room.players.splice(0, 1);
		  		room.players[0].p = 1;
		  	}
		  	else
		  	{
		  		room.players.splice(1, 1);
		  	}
		}
	  	
  	});

});

//app.use(express.static('public'));   //browsers should be able to play against app users.

app.listen(PORT);

console.log('listening on ' + PORT);

setInterval(() => {
	 Object.keys(rooms).forEach((key) => {
	 	rooms[key].players.forEach((player) => {
	 		player.ws.send(makeMessage('ping', 'data'));
	 	});
	 });
}, 30000)

